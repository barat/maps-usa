from bokeh.plotting import figure, show, output_file
from bokeh.models import CustomJS, MultiSelect
from bokeh.palettes import Viridis6 as palette
from bokeh.models import LogColorMapper, LinearColorMapper, ColorBar
from bokeh.models import (CDSView, ColorBar, ColumnDataSource,
                          CustomJS, CustomJSFilter,
                          GeoJSONDataSource, HoverTool, Slider)
from bokeh.layouts import column, row, widgetbox
from bokeh.palettes import brewer
from bokeh.plotting import figure
from bokeh.sampledata.us_counties import data as counties
from bokeh.sampledata.us_states import data as states
from bokeh.sampledata.unemployment import data as unemployment
import csv
import json
import geopandas as gpd
import pandas as pd
import numpy as np



EXCLUDED = ("ak", "hi", "pr", "gu", "vi", "mp", "as")
OPTIONS = [states[xx]["name"] for xx in states]
state_xs = [states[code]["lons"] for code in states]
state_ys = [states[code]["lats"] for code in states]

county_xs=[counties[code]["lons"] for code in counties if counties[code]["state"] not in EXCLUDED]
county_ys=[counties[code]["lats"] for code in counties if counties[code]["state"] not in EXCLUDED]


# MICHIGAN

data_mi={}
total = 0
i = 0
with open('data/michigan.csv') as f:
    #has_header = csv.Sniffer().has_header(f.read(1024))
    #f.seek(0)  # Rewind.
    reader = csv.reader(f, delimiter=",", quotechar='"')
    #if has_header:
    #    next(reader)  # Skip header row
    for row in reader:
        dummy, dummy, dummy, dummy, county_id, county_name, dummy, dummy, party, dummy, dummy, candidate, dummy, dummy, dummy, votes, dummy, dummy, dummy = row
        if candidate == "Peters":
            data_mi[(int(county_id), 1)] = float(votes)
            i += 1
        elif candidate == "James":
            data_mi[(int(county_id), 2)] = float(votes)
            #total += float(votes)

try_panda_DS = pd.read_csv('data/michigan.csv', usecols=['CountyCode', 'CandidateLastName', 'CandidateVotes'])
try_panda_DS = try_panda_DS.loc[try_panda_DS['CandidateLastName'].isin(['Peters', 'James'])]
try_panda_D = try_panda_DS.loc[try_panda_DS['CandidateLastName'].isin(['Peters'])]
#try_panda_D = try_panda_D.drop(columns=['CandidateLastName'])
try_panda_R = try_panda_DS.loc[try_panda_DS['CandidateLastName'].isin(['James'])]
#try_panda_R = try_panda_R.drop(columns=['CandidateLastName'])
sum_mi = try_panda_R.reset_index()['CandidateVotes'] + try_panda_D.reset_index()['CandidateVotes']
#try_panda_D['CandidateVotes'] = try_panda_D['CandidateVotes'] * 100 / sum_mi
#diff_panda = try_panda_D.reset_index()['CandidateVotes'] - try_panda_R.reset_index()['CandidateVotes']
diff_panda = try_panda_D.reset_index()['CandidateVotes'] - try_panda_R.reset_index()['CandidateVotes']
diff_panda = diff_panda * 100 / sum_mi
diff_panda = diff_panda.to_frame()
diff_panda.columns = ["Voti"]
diff_panda['Voti'] = diff_panda['Voti'].astype('str')
#  diff_panda.columns = pd.MultiIndex.from_tuples(zip(['AA', 'BB'], diff_panda.columns))
#  diff_panda.columns = pd.MultiIndex.from_tuples(zip(['AA', 'BB'], diff_panda.columns))
#diff_panda = try_panda_D['CandidateVotes'] - try_panda_R['CandidateVotes']

michigan_try = gpd.read_file('data/cb_2018_us_county_500k.shp')
#michigan_try.head()

michigan_try = michigan_try.loc[michigan_try['STATEFP'].isin(['26'])]
michigan_try = michigan_try.sort_values('NAME', ascending=True)
michigan_try.index = pd.RangeIndex(len(michigan_try.index))
#merged_mi = michigan_try.merge(diff_panda, left_on = 'AWATER', right_on = 'Voti')
merged_mi = michigan_try.join(diff_panda, how='left')
merged_json = json.loads(merged_mi.to_json())
geo_mi_data = json.dumps(merged_json)
geo_michigan = GeoJSONDataSource(geojson = geo_mi_data)

# Texas

data_tx={}
f = pd.read_excel(open('data/texas_senate.xlsx', 'rb'), sheet_name='Voter Turnout Report')
f = f.drop(columns=['ELECTION DATE-NAME'], axis=1)
tx_cornyn = f.loc[f['CANDIDATE NAME'].isin(['JOHN CORNYN'])]
tx_cornyn = tx_cornyn.drop(columns=['OFFICE NAME'], axis=1)
tx_hegar = f.loc[f['CANDIDATE NAME'].isin(['MARY "MJ" HEGAR'])]
tx_hegar = tx_hegar.drop(columns=['OFFICE NAME', 'CANDIDATE NAME'], axis=1)
print(tx_hegar)
# with open('data/texas_senate.xlsx') as f:
#     #has_header = csv.Sniffer().has_header(f.read(1024))
#     #f.seek(0)  # Rewind.
#     reader = csv.reader(f, delimiter=",", quotechar='"')
#     #if has_header:
#     #    next(reader)  # Skip header row
#     for row in reader:
#         dummy, dummy, dummy, dummy, county_id, county_name, dummy, dummy, party, dummy, dummy, candidate, dummy, dummy, dummy, votes, dummy, dummy, dummy = row
#         if candidate == "Peters":
#             data_mi[(int(county_id), 1)] = float(votes)
#             i += 1
#         elif candidate == "James":
#             data_mi[(int(county_id), 2)] = float(votes)
#             #total += float(votes)
#
# try_panda_DS = pd.read_csv('data/michigan.csv', usecols=['CountyCode', 'CandidateLastName', 'CandidateVotes'])
# try_panda_DS = try_panda_DS.loc[try_panda_DS['CandidateLastName'].isin(['Peters', 'James'])]
# try_panda_D = try_panda_DS.loc[try_panda_DS['CandidateLastName'].isin(['Peters'])]
# #try_panda_D = try_panda_D.drop(columns=['CandidateLastName'])
# try_panda_R = try_panda_DS.loc[try_panda_DS['CandidateLastName'].isin(['James'])]
# #try_panda_R = try_panda_R.drop(columns=['CandidateLastName'])
# sum_mi = try_panda_R.reset_index()['CandidateVotes'] + try_panda_D.reset_index()['CandidateVotes']
# #try_panda_D['CandidateVotes'] = try_panda_D['CandidateVotes'] * 100 / sum_mi
# #diff_panda = try_panda_D.reset_index()['CandidateVotes'] - try_panda_R.reset_index()['CandidateVotes']
# diff_panda = try_panda_D.reset_index()['CandidateVotes'] - try_panda_R.reset_index()['CandidateVotes']
# diff_panda = diff_panda * 100 / sum_mi
# diff_panda = diff_panda.to_frame()
# diff_panda.columns = ["Voti"]
# print(diff_panda)
# diff_panda['Voti'] = diff_panda['Voti'].astype('str')
# print(diff_panda)
# #  diff_panda.columns = pd.MultiIndex.from_tuples(zip(['AA', 'BB'], diff_panda.columns))
# #  diff_panda.columns = pd.MultiIndex.from_tuples(zip(['AA', 'BB'], diff_panda.columns))
# #diff_panda = try_panda_D['CandidateVotes'] - try_panda_R['CandidateVotes']
#
# michigan_try = gpd.read_file('data/cb_2018_us_county_500k.shp')
# #michigan_try.head()
#
# michigan_try = michigan_try.loc[michigan_try['STATEFP'].isin(['26'])]
# michigan_try = michigan_try.sort_values('NAME', ascending=True)
# michigan_try.index = pd.RangeIndex(len(michigan_try.index))
#
# print(michigan_try.head())
# print(michigan_try.info())
#
# #merged_mi = michigan_try.merge(diff_panda, left_on = 'AWATER', right_on = 'Voti')
# merged_mi = michigan_try.join(diff_panda, how='left')
# print(merged_mi.head())
# print(merged_mi.info())
# merged_json = json.loads(merged_mi.to_json())
# geo_mi_data = json.dumps(merged_json)
# geo_michigan = GeoJSONDataSource(geojson = geo_mi_data)






counties_mi = {
            code: county for code, county in counties.items() if county["state"] == "mi"
        }
county_xs_MI = [county["lons"] for county in counties_mi.values()]
county_ys_MI = [county["lats"] for county in counties_mi.values()]
county_names_MI = [county["name"] for county in counties_mi.values()]
county_id_MI = [county["id_county"] for county in counties_mi.values()]
#print(county_id_MI)
county_1 = [data_mi[(ii+1, 1)] for ii in range(i)]
county_2 = [data_mi[(ii+1, 2)] for ii in range(i)]



data_plot_mi=dict(x=county_xs_MI,
                  y=county_ys_MI,
                  name=county_names_MI,
                  peters=county_1,
                  james=county_2,
                  difference = diff_panda)

data_new = {}
with open('data/try.csv') as f:
    has_header = csv.Sniffer().has_header(f.read(1024))
    f.seek(0)  # Rewind.
    reader = csv.reader(f, delimiter=",", quotechar='"')
    if has_header:
        next(reader)  # Skip header row
    for row in reader:
        county, county_id, dummy, dummy, dummy, dummy, perdue, dummy, dummy, dummy, dummy, ossof,dummy, dummy, dummy, dummy, lib, tot  = row
        data_new[int(county_id), 1] = float(perdue)/float(tot)*100
        data_new[int(county_id), 2] = float(ossof)/float(tot)*100

data_mich = pd.DataFrame.from_dict(data_mi, orient='index', columns=['Candidate'])
data_mich_peters= data_mich

palette = brewer['RdBu'][10]
palette = palette[::-1]


contiguous_states_usa = gpd.read_file('data/cb_2018_us_state_500k.shp')
contiguous_states_usa.head()
contiguous_states_usa = contiguous_states_usa.loc[~contiguous_states_usa['STATEFP'].isin(['02', '15', '66', '60', '78', '69', '72'])]


#print(geo_michigan['geometry'])


contiguous_usa = gpd.read_file('data/cb_2018_us_county_500k.shp')
contiguous_usa.head()
contiguous_usa = contiguous_usa.loc[~contiguous_usa['STATEFP'].isin(['02', '15', '66', '60', '78', '69', '72'])]

geosource_states = GeoJSONDataSource(geojson = contiguous_states_usa.to_json())
geosource = GeoJSONDataSource(geojson = contiguous_usa.to_json())

counties_ga = {
            code: county for code, county in counties.items() if county["state"] == "ga"
        }

#county_xs, county_ys = geo_michigan['coordinates']
county_xs = [county["lons"] for county in counties_ga.values()]
#county_ys = [county["lats"] for county in counties_ga.values()]
county_ys = [county["lats"] for county in counties_ga.values()]
county_names = [county['name'] for county in counties_ga.values()]

county_perdue = [data_new[(int(county["id_county"]), 1)] for county in counties_ga.values()]
county_ossof = [data_new[(int(county["id_county"]), 2)] for county in counties_ga.values()]

dff=[i for i in range(len(county_ossof)) ]
for i in range(len(county_ossof)):
    dff[i] = county_ossof[i] - county_perdue[i]
        #print(dff)

data=dict(x=county_xs,
          y=county_ys,
          name=county_names,
          perdue=county_perdue,
          ossof=county_ossof,
          difference = dff)


#
# county_colors = []
# for county_id in counties:
#     if counties[county_id]["state"] in EXCLUDED:
#         continue
#     try:
#         rate = unemployment[county_id]
#         idx = int(rate/6)
#         county_colors.append(colors[idx])
#     except KeyError:
#         county_colors.append("black")
TOOLS = "pan,wheel_zoom,reset,hover,save"
tick_labels = {'0': 'R>+80%', '10': 'R+60%', '20':'R+40%', '30':'R+20%'}

p = figure(title="US Unemployment 2009", toolbar_location="left", tools=TOOLS,
           plot_width=1100, plot_height=700)
p.grid.grid_line_color = None
p.hover.point_policy = "follow_mouse"
#p.patches(county_xs, county_ys,
#          fill_color={'field': 'difference', 'transform': color_mapper}, fill_alpha=0.7,
#          line_color="white", line_width=0.5)
#p.patches(state_xs, state_ys,
#          fill_alpha=0.7,
#          line_color="white", line_width=0.5)

states = p.patches('xs','ys', source = geosource, fill_color='white', line_color = 'black', line_width = 0.25, fill_alpha = 1)




#multi_select = MultiSelect(value=["1", "2"], options=OPTIONS)
# multi_select.js_on_change("value", CustomJS(code="""
#     var
# """))
# data={}
# def update(attr, old, new):
#     if new == "GA":
#         counties_ga = {
#             code: county for code, county in counties.items() if county["state"] == "ga"
#         }
#         county_xs = [county["lons"] for county in counties_ga.values()]
#         county_ys = [county["lats"] for county in counties_ga.values()]
#         county_names = [county['name'] for county in counties_ga.values()]
#
#         county_perdue = [data_new[(int(county["id_county"]), 1)] for county in counties_ga.values()]
#         county_ossof = [data_new[(int(county["id_county"]), 2)] for county in counties_ga.values()]
#
#         dff=[i for i in range(len(county_ossof)) ]
#         for i in range(len(county_ossof)):
#             dff[i] = county_ossof[i] - county_perdue[i]
#         #print(dff)
#
#         data=dict(
#             x=county_xs,
#             y=county_ys,
#             name=county_names,
#             perdue=county_perdue,
#             ossof=county_ossof,
#             difference = dff
#         )
color_mapper = LinearColorMapper(palette=palette, low=-80, high=+80)



ga = p.patches('x', 'y', source=data, fill_color={'field': 'difference', 'transform': color_mapper}, fill_alpha=0.7, line_color="white", line_width=0.5)
#mi = p.patches('x', 'y', source=data_plot_mi, fill_color={'field': 'difference', 'transform': color_mapper}, fill_alpha=0.7, line_color="white", line_width=0.5)
mi = p.patches('xs', 'ys', source=geo_michigan, fill_color={'field': 'Voti', 'transform': color_mapper}, fill_alpha=0.7, line_color="white", line_width=0.5)
# callback = CustomJS(code=code, args={})
# checkbox = CheckboxGroup(labels=["Line 0", "Line 1", "Line 2"], active=[0, 1, 2], callback=callback, width=100)
# callback.args = dict(l0=l0, l1=l1, l2=l2, checkbox=checkbox)
#show(multi_select)
# Make a column layout of widgetbox(slider) and plot, and add it to the current document
#multi_select.on_change("value", update)
states1 = p.patches('xs','ys', source = geosource_states, fill_color='white', line_color = 'black', line_width = 0.5, fill_alpha = 0)

layout = column(p)

output_file("choropleth.html", title="choropleth.py example")



show(layout)
